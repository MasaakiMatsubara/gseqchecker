package org.glycoinfo.GSeqConsistencyChecker.test;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.SequenceRegistrationInspection.reporter.io.ReportWriter;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.DuplicationReport;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.IReport;

public class TestReportWriter {

	public static void main(String[] argv) {
		List<IReport> repos = new ArrayList<>();
		DuplicationReport repo = new DuplicationReport();
		repo.setSequence("sequence");
		repo.setDuplicatedIDs(new ArrayList<>());
		repo.getDuplicatedIDs().add("1");
		repo.getDuplicatedIDs().add("2");
		repos.add(repo);

		ReportWriter writer = new ReportWriter();
		writer.setReports(repos);
		writer.writeReports("repo.txt");
	}
}
