package org.glycoinfo.GSeqConsistencyChecker.test;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.GSeqConsistencyChecker.SequenceLoader;
import org.glycoinfo.SequenceRegistrationInspection.reporter.conversion.GlycoCTToWURCSReporter;
import org.glycoinfo.SequenceRegistrationInspection.reporter.io.ReportWriter;
import org.glycoinfo.SequenceRegistrationInspection.reporter.normalization.GlycoCTNormalizationReporter;
import org.glycoinfo.SequenceRegistrationInspection.reporter.normalization.WURCSNormalizationReporter;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.ConversionReport;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.IReport;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.NormalizationReport;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.ValidationReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestGlycoCTInspection {

	private static final Logger logger = LoggerFactory.getLogger(TestGlycoCTInspection.class);

	private static List<String> statistics;

	public static void main(String[] args) throws IOException {
		if ( args.length != 1 ) {
			System.out.println("<This program> <input>");
			return;
		}

		String inFile = args[0];
		String outDir = inFile.replaceAll("\\..*$", "");
		File out = new File(outDir);
		if ( !out.isDirectory() && !out.mkdir() ) {
			System.out.println("Failed to make output directory.");
			return;
		}

		logger.info("Load sequences (GlyTouCanID WURCS GlycoCT)");

		SequenceLoader.load(Paths.get(inFile));

		// Statistics
		statistics = new ArrayList<>();
		statistics.add("Total entries:\t"+SequenceLoader.getIDs().size());
		statistics.add("Total unique WURCSs:\t"+SequenceLoader.getWURCSs().size());
		statistics.add("Total unique GlycoCTs:\t"+SequenceLoader.getSeqs().size());

		// Check WURCS
		logger.info("Start checking WURCS normalization:");
		WURCSNormalizationReporter normWURCS = new WURCSNormalizationReporter();
		for ( String wurcs : SequenceLoader.getWURCSs() )
			normWURCS.process(wurcs);

		logger.info("Start checking GlycoCT normalization:");
		GlycoCTNormalizationReporter normGCT = new GlycoCTNormalizationReporter();
		for ( String gct : SequenceLoader.getSeqs() )
			normGCT.process(gct);

		logger.info("Start checking GlycoCT conversion to WURCS:");
		GlycoCTToWURCSReporter repoConv = new GlycoCTToWURCSReporter(normGCT, normWURCS);
		for (String id : SequenceLoader.getIDs()) {
			repoConv.process(id);
		}

		// Result output
		statistics.add("");
		List<IReport> repos = new ArrayList<>();
		writerReports(outDir, "Normalized_WURCS.tsv", normWURCS.getReports());
		for ( NormalizationReport repo : normWURCS.getReports() )
			if ( repo.getIDsForNormalized() != null )
				repos.add(repo);
		writerReports(outDir, "Normalized_Matched_WURCS.tsv", repos);
		writerReports(outDir, "Duplicated_WURCS.tsv", normWURCS.getDuplicationReports());
		writerReports(outDir, "Validation_WURCS.tsv", normWURCS.getValidationReports());

		writerReports(outDir, "Normalized_GlycoCT.tsv", normGCT.getReports());
		repos.clear();
		for ( NormalizationReport repo : normGCT.getReports() )
			if ( repo.getIDsForNormalized() != null )
				repos.add(repo);
		writerReports(outDir, "Normalized_Matched_GlycoCT.tsv", repos);
		writerReports(outDir, "Duplicated_GlycoCT.tsv", normGCT.getDuplicationReports());
		writerReports(outDir, "Validation_GlycoCT.tsv", normGCT.getValidationReports());

		writerReports(outDir, "Conversion_GlycoCTToWURCS.tsv", repoConv.getReports());
		repos.clear();
		for ( ConversionReport repo : repoConv.getReports() ) {
			if ( repo.getIDsForToSequence(false) != null )
				repos.add(repo);
		}
		writerReports(outDir, "Conversion_Matched_GlycoCTToWURCS.tsv", repos);
		repos.clear();
		for ( ConversionReport repo : repoConv.getReports() ) {
			if ( repo.getIDsForToSequence(true) != null )
				repos.add(repo);
		}
		writerReports(outDir, "Conversion_MatchedToNormalized_GlycoCTToWURCS.tsv", repos);
		repos.clear();
		for ( ConversionReport repo : repoConv.getReports() ) {
			if ( repo.getToOriginalSequence().contains("0+")
			 && !repo.getToSequence().contains("0+") )
				repos.add(repo);
		}
		writerReports(outDir, "Conversion_CompostionNormalized_GlycoCTToWURCS.tsv", repos);
		writerReports(outDir, "Validation_GlycoCTToWURCS.tsv", repoConv.getValidationReports());
		repos.clear();
		for ( ValidationReport repo : repoConv.getValidationReports() ) {
			if ( repo.getMessages().contains("No WURCS") )
				repos.add(repo);
		}
		writerReports(outDir, "Validation_NoWURCS_GlycoCTToWURCS.tsv", repos);

		// Write statistics
		String statisticsPath = outDir+File.separator+"statistics.txt";
		try (BufferedOutputStream bufferedOutputStream =
				new BufferedOutputStream(new FileOutputStream(new File(statisticsPath)))) {
			for ( String line : statistics) {
				bufferedOutputStream.write(line.getBytes());
				bufferedOutputStream.write(System.getProperty("line.separator").getBytes());
			}
		} catch (IOException e) {
			logger.error("An error in writing statistics to "+statisticsPath, e);
		}

	}

	public static void writerReports(String fileDir, String fileName, List<? extends IReport> repos) {
		statistics.add(fileName+":\t"+repos.size());
		ReportWriter writer = new ReportWriter();
		writer.setReports(repos);
		writer.writeReports(fileDir+File.separator+fileName);
	}
}
