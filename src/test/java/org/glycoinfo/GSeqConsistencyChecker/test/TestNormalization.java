package org.glycoinfo.GSeqConsistencyChecker.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.GSeqConsistencyChecker.SequenceLoader;
import org.glycoinfo.SequenceRegistrationInspection.reporter.io.ReportWriter;
import org.glycoinfo.SequenceRegistrationInspection.reporter.normalization.WURCSNormalizationReporter;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.IReport;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.NormalizationReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestNormalization {
	private static final Logger logger = LoggerFactory.getLogger(TestNormalization.class);

	private static List<String> statistics;

	public static void main(String[] args) throws IOException {
		if ( args.length != 1 ) {
			System.out.println("<This program> <input>");
			return;
		}

		String inFile = args[0];
		String outDir = inFile.replaceAll("\\..*$", "");
		File out = new File(outDir);
		if ( !out.isDirectory() && !out.mkdir() ) {
			System.out.println("Failed to make output directory.");
			return;
		}

		logger.info("Load sequences (GlyTouCanID WURCS)");

		SequenceLoader.load(Paths.get(inFile));

		// Statistics
		statistics = new ArrayList<>();
		statistics.add("Total entries:\t"+SequenceLoader.getIDs().size());
		statistics.add("Total unique WURCSs:\t"+SequenceLoader.getWURCSs().size());

		// Check WURCS
		logger.info("Start checking WURCS normalization:");
		WURCSNormalizationReporter normWURCS = new WURCSNormalizationReporter();
		for ( String wurcs : SequenceLoader.getWURCSs() )
			normWURCS.process(wurcs);

		// Result output
		statistics.add("");
		List<IReport> repos = new ArrayList<>();
		writerReports(outDir, "Normalized_WURCS.tsv", normWURCS.getReports());
		for ( NormalizationReport repo : normWURCS.getReports() )
			if ( repo.getIDsForNormalized() != null )
				repos.add(repo);
		writerReports(outDir, "Normalized_Matched_WURCS.tsv", repos);
		writerReports(outDir, "Duplicated_WURCS.tsv", normWURCS.getDuplicationReports());
		writerReports(outDir, "Validation_WURCS.tsv", normWURCS.getValidationReports());
	}

	public static void writerReports(String fileDir, String fileName, List<? extends IReport> repos) {
		statistics.add(fileName+":\t"+repos.size());
		ReportWriter writer = new ReportWriter();
		writer.setReports(repos);
		writer.writeReports(fileDir+File.separator+fileName);
	}

}
