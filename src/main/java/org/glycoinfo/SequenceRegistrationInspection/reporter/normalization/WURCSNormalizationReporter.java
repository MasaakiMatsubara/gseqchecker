package org.glycoinfo.SequenceRegistrationInspection.reporter.normalization;

import java.io.IOException;
import java.util.List;

import org.glycoinfo.GSeqConsistencyChecker.SequenceLoader;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.validation.WURCSValidationReport;
import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;

public class WURCSNormalizationReporter extends NormalizationReporter {

	public WURCSNormalizationReporter() {
		super("WURCS");
	}

	@Override
	protected List<String> getIDs(String wurcs) throws IOException {
		return SequenceLoader.getIDsForWURCS(wurcs);
	}

	@Override
	protected String normalize(String wurcs) throws Exception {
		WURCSFactory factory = new WURCSFactory(wurcs);
		return factory.getWURCS();
	}

	@Override
	protected String validate(String wurcs) {
		WURCSValidator validator = new WURCSValidator();
		validator.start(wurcs);
		WURCSValidationReport repo = validator.getReport();
		return repo.getResults();
	}
}
