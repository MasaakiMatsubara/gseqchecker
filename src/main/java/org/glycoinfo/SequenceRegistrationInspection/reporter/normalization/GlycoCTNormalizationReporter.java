package org.glycoinfo.SequenceRegistrationInspection.reporter.normalization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarExporterGlycoCTCondensed;
import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarImporterGlycoCTCondensed;
import org.eurocarbdb.MolecularFramework.io.namespace.GlycoVisitorToGlycoCT;
import org.eurocarbdb.MolecularFramework.sugar.Sugar;
import org.eurocarbdb.MolecularFramework.util.validation.GlycoVisitorValidation;
import org.eurocarbdb.resourcesdb.Config;
import org.eurocarbdb.resourcesdb.io.MonosaccharideConverter;
import org.glycoinfo.GSeqConsistencyChecker.SequenceLoader;
import org.glycoinfo.WURCSFramework.io.GlycoCT.GlycoVisitorValidationForWURCS;

public class GlycoCTNormalizationReporter extends NormalizationReporter {

	public GlycoCTNormalizationReporter() {
		super("GlycoCT");
	}

	@Override
	protected List<String> getIDs(String gct) throws IOException {
		return SequenceLoader.getIDsForSequence(gct);
	}

	@Override
	protected String normalize(String gct) throws Exception {
		SugarImporterGlycoCTCondensed importSugar = new SugarImporterGlycoCTCondensed();
		Sugar sugar = importSugar.parse(gct);
		// Normalize sugar
		GlycoVisitorToGlycoCT normalize
			= new GlycoVisitorToGlycoCT( new MonosaccharideConverter( new Config() ) );
		normalize.start(sugar);
		sugar = normalize.getNormalizedSugar();

		SugarExporterGlycoCTCondensed export = new SugarExporterGlycoCTCondensed();
		export.start(sugar);

		return export.getHashCode().trim();
	}

	@Override
	protected String validate(String gct) {
		List<String> t_aErrorStrings   = new ArrayList<>();
		List<String> t_aWarningStrings = new ArrayList<>();
		try {
			SugarImporterGlycoCTCondensed importSugar = new SugarImporterGlycoCTCondensed();
			Sugar sugar = importSugar.parse(gct);

			GlycoVisitorValidation t_validation = new GlycoVisitorValidation();
			t_validation.start(sugar);
			t_aErrorStrings.addAll(   t_validation.getErrors() );
			t_aWarningStrings.addAll( t_validation.getWarnings() );

			// Remove error "Sugar has more than one root residue." for handling compositions
			while( t_aErrorStrings.contains("Sugar has more than one root residue.") )
				t_aErrorStrings.remove( t_aErrorStrings.indexOf("Sugar has more than one root residue.") );

			// Validate for WURCS
			GlycoVisitorValidationForWURCS t_validationWURCS = new GlycoVisitorValidationForWURCS();
			t_validationWURCS.start(sugar);

			// Marge errors and warnings
			t_aErrorStrings.addAll(   t_validationWURCS.getErrors() );
			t_aWarningStrings.addAll( t_validationWURCS.getWarnings() );

		} catch (Exception e) {
			String strMessage = String.format("[%s] %s", e.getClass().getSimpleName(), e.getMessage());
			t_aErrorStrings.add(strMessage);
		}

		StringBuffer sbLog = new StringBuffer();

		if ( !t_aErrorStrings.isEmpty() )
			sbLog.append("Errors:\n");
		for ( String err : t_aErrorStrings )
			sbLog.append(err+"\n");

		if ( !t_aWarningStrings.isEmpty() )
			sbLog.append("Warnings:\n");
		for ( String warn : t_aWarningStrings ) {
			sbLog.append(warn+"\n");
		}

		return sbLog.toString();
	}

}
