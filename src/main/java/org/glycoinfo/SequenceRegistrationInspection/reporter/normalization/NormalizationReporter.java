package org.glycoinfo.SequenceRegistrationInspection.reporter.normalization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.glycoinfo.SequenceRegistrationInspection.reporter.om.DuplicationReport;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.NormalizationReport;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.ValidationReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class NormalizationReporter {
	private static final Logger logger = LoggerFactory.getLogger(NormalizationReporter.class);

	private String format;

	private List<NormalizationReport> lReports;
	private List<DuplicationReport> lDuplicates;
	private List<ValidationReport> lValidations;

	private Map<String, NormalizationReport> mapOrigToReport;
	private Map<String, NormalizationReport> mapNormToReport;
	private List<String> lErrorSequences;

	public NormalizationReporter(String format) {
		this.format = format;

		this.lReports = new ArrayList<>();
		this.lDuplicates = new ArrayList<>();
		this.lValidations = new ArrayList<>();
		this.mapOrigToReport = new HashMap<>();
		this.mapNormToReport = new HashMap<>();
		this.lErrorSequences = new ArrayList<>();
	}

	public List<NormalizationReport> getReports() {
		return this.lReports;
	}

	public List<DuplicationReport> getDuplicationReports() {
		return this.lDuplicates;
	}

	public List<ValidationReport> getValidationReports() {
		return this.lValidations;
	}

	public String getOriginalSequence(String seq) {
		if ( seq == null || seq.isEmpty() || !this.mapNormToReport.containsKey(seq) )
			return null;
		return this.mapNormToReport.get(seq).getOriginalSequence();
	}

	public String getNormalizedSequence(String seq) {
		if ( seq == null || seq.isEmpty() || !this.mapOrigToReport.containsKey(seq) )
			return null;
		return this.mapOrigToReport.get(seq).getNormalizedSequence();
	}

	public List<String> getOriginalIDs(String seq) {
		if ( seq == null || seq.isEmpty() || !this.mapNormToReport.containsKey(seq) )
			return null;
		return this.mapNormToReport.get(seq).getIDsForOriginal();
	}

	public List<String> getNormalizedIDs(String seq) {
		if ( seq == null || seq.isEmpty() || !this.mapOrigToReport.containsKey(seq) )
			return null;
		return this.mapOrigToReport.get(seq).getIDsForNormalized();
	}

	public boolean isError(String seq) {
		return this.lErrorSequences.contains(seq);
	}

	public void process(String seq) throws IOException {
		List<String> ids = getIDs(seq);
		if ( ids.size() > 1 ) {
			logger.warn("This {} is duplicated in {}:\n{}", format, ids, seq);
			DuplicationReport dupRepo = new DuplicationReport();
			dupRepo.setSequence(seq);
			dupRepo.setDuplicatedIDs(ids);
			this.lDuplicates.add(dupRepo);
		}

		// Validate sequence
		String result = validate(seq);
		if ( result != null && !result.isEmpty() ) {
			logger.error("This {} of {} has validation result:\n{}", format, ids, seq);
			ValidationReport valRep = new ValidationReport();
			valRep.setSequence(seq);
			valRep.setIDs(ids);
			valRep.setMessages(result);
			this.lValidations.add(valRep);
		}

		// Normalize sequence
		String seqNew = null;
		try {
			seqNew = normalize(seq);
		} catch (Exception e) {
			logger.error("This {} of {} has normalization error:\n{}", format, ids, seq);
			logger.error(e.getMessage(), e);
			lErrorSequences.add(seq);
		}

		if ( seqNew == null || seq.equals(seqNew) )
			return;

		String sep = ( seq.contains("\n") )? "\n" : "\t";
		logger.info("{} has been normalized:", format);
		logger.info("From:"+sep+seq);
		logger.info("To:"+sep+seqNew);

		NormalizationReport report = new NormalizationReport();
		report.setOriginalSequence(seq);
		report.setNormalizedSequence(seqNew);
		report.setIDsForOriginal(ids);

		this.lReports.add(report);
		this.mapOrigToReport.put(seq, report);
		this.mapNormToReport.put(seqNew, report);

		// Collect ids for normalized WURCS
		ids = getIDs(seqNew);
		if ( ids != null ) {
			logger.warn("Normalized {} is already registered in {}", format, ids);
			report.setIDsForNormalized(ids);
		}
	}

	protected abstract List<String> getIDs(String seqInput) throws IOException;
	protected abstract String validate(String seqInput);
	protected abstract String normalize(String seqInput) throws Exception;
}
