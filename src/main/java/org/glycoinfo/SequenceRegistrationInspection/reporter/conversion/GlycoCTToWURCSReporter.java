package org.glycoinfo.SequenceRegistrationInspection.reporter.conversion;

import java.io.IOException;
import java.util.List;

import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.glycoinfo.GSeqConsistencyChecker.SequenceLoader;
import org.glycoinfo.SequenceRegistrationInspection.reporter.normalization.GlycoCTNormalizationReporter;
import org.glycoinfo.SequenceRegistrationInspection.reporter.normalization.WURCSNormalizationReporter;
import org.glycoinfo.WURCSFramework.io.GlycoCT.WURCSExporterGlycoCT;
import org.glycoinfo.WURCSFramework.util.WURCSException;

public class GlycoCTToWURCSReporter extends ConversionReporter {
	private static WURCSExporterGlycoCT conv;

	public GlycoCTToWURCSReporter(GlycoCTNormalizationReporter normGCT, WURCSNormalizationReporter normWURCS) {
		super("GlycoCT", "WURCS", normGCT, normWURCS);
	}

	@Override
	protected List<String> getFromSequences(String id) throws IOException {
		return SequenceLoader.getSequences(id);
	}

	@Override
	protected List<String> getToSequences(String id) throws IOException {
		return SequenceLoader.getWURCSs(id);
	}

	@Override
	protected List<String> getIDsForToSequence(String toSeq) throws IOException {
		return SequenceLoader.getIDsForWURCS(toSeq);
	}

	@Override
	protected String convert(String gct) throws SugarImporterException, GlycoVisitorException, WURCSException {
		conv = new WURCSExporterGlycoCT();
		conv.start(gct);
		return conv.getWURCS();
	}

	@Override
	protected String getValidationMessages() {
		return conv.getValidationErrorLog().toString();
	}
}
