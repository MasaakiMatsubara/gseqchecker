package org.glycoinfo.SequenceRegistrationInspection.reporter.conversion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.SequenceRegistrationInspection.reporter.normalization.NormalizationReporter;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.ConversionReport;
import org.glycoinfo.SequenceRegistrationInspection.reporter.om.ValidationReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ConversionReporter {
	private static final Logger logger = LoggerFactory.getLogger(ConversionReporter.class);

	private List<ConversionReport> lConvRepos;
	private List<ValidationReport> lValidRepos;

	private String formatFrom;
	private String formatTo;

	private NormalizationReporter normFrom;
	private NormalizationReporter normTo;

	public ConversionReporter(String formatFrom, String formatTo,
			NormalizationReporter normFrom, NormalizationReporter normTo) {
		this.formatFrom = formatFrom;
		this.formatTo = formatTo;
		this.normFrom = normFrom;
		this.normTo = normTo;

		this.lConvRepos = new ArrayList<>();
		this.lValidRepos = new ArrayList<>();
	}

	public List<ConversionReport> getReports() {
		return this.lConvRepos;
	}

	public List<ValidationReport> getValidationReports() {
		return this.lValidRepos;
	}

	public String getFromFromat() {
		return this.formatFrom;
	}

	public String getToFormat() {
		return this.formatTo;
	}

	public NormalizationReporter getFromNormalizationReporter() {
		return this.normFrom;
	}

	public NormalizationReporter getToNormalizationReporter() {
		return this.normTo;
	}

	public void process(String id) throws IOException {
		// Prepare for "from" sequences
		List<String> lFromSeqs = getFromSequences(id);
		if ( lFromSeqs == null ) {
			logger.warn("No {} for {}",formatTo, id);
			lFromSeqs = new ArrayList<>();
		}
		// Update "From" sequences to normalized
		List<String> lFromSeqsNorm = new ArrayList<>();
		for ( String toSeq : lFromSeqs ) {
			String toSeqNorm = getFromNormalizationReporter().getNormalizedSequence(toSeq);
			if ( toSeqNorm != null )
				toSeq = toSeqNorm;
			lFromSeqsNorm.add(toSeq);
		}
		lFromSeqs = lFromSeqsNorm;

		// Prepare for "to" sequences
		List<String> lToSeqs = getToSequences(id);
		if ( lToSeqs == null ) {
			logger.warn("No {} for {}",formatTo, id);
			lToSeqs = new ArrayList<>();
		}
		// Update "to" sequences to normalized
		List<String> lToSeqsNorm = new ArrayList<>();
		for ( String toSeq : lToSeqs ) {
			String toSeqNorm = getToNormalizationReporter().getNormalizedSequence(toSeq);
			if ( toSeqNorm != null )
				toSeq = toSeqNorm;
			lToSeqsNorm.add(toSeq);
		}
		lToSeqs = lToSeqsNorm;
		if ( lToSeqs.isEmpty() )
			lToSeqs.add(null);

		for ( String seqFrom : lFromSeqs ) {
			try {
				String seqToConv = convert(seqFrom);

				if ( lToSeqs.contains(seqToConv) )
					continue;
				for ( String seqToOrig : lToSeqs ) {
					ConversionReport repo = makeReport(id, seqFrom, seqToConv, seqToOrig);
					this.lConvRepos.add(repo);
				}
			} catch (Exception e) {
				logger.error("Error in {} conversion: {}", formatFrom, id);
				logger.error(e.getMessage(), e);
				ValidationReport repoValid = new ValidationReport();
				repoValid.setSequence(seqFrom);
				repoValid.setIDs(new ArrayList<>());
				repoValid.getIDs().add(id);
				repoValid.setException(e);
				String messages = getValidationMessages();
				if ( lToSeqs.contains(null) )
					messages += "No WURCS\n";
				repoValid.setMessages( messages );
				this.lValidRepos.add(repoValid);
			}
		}
	}

	private String getSeparator(String seq) {
		return (seq.contains("\n"))? "\n" : "\t";
	}

	private ConversionReport makeReport(String id, String seqFrom, String seqTo, String seqToOrig) throws IOException {
		ConversionReport repo = new ConversionReport();
		repo.setID(id);
		repo.setFromSequence(seqFrom);
		repo.setToSequence(seqTo);
		repo.setToOriginalSequence(seqToOrig);


		if ( seqToOrig != null ) {
			String sepFrom = getSeparator(seqFrom);
			String sepTo = getSeparator(seqTo);
			String sepToOrig = getSeparator(seqToOrig);

			logger.info("WURCS has been changed:");
			logger.info("{}:{}{}", formatFrom, sepFrom, seqFrom);
			logger.info("ID:\t"+id);
			logger.info("Current {}:{}{}", formatTo, sepToOrig, seqToOrig);
			logger.info("Converted {}:{}{}", formatTo, sepTo, seqTo);
		}

		List<String> idsTo = getIDsForToSequence(seqTo);
		if ( idsTo != null && !idsTo.isEmpty() && !idsTo.contains(id) ) {
			logger.info("The converted {} is already registered in {}", formatTo, idsTo);
			repo.setIDsForToSequence(idsTo, false);
		}
		idsTo = getToNormalizationReporter().getNormalizedIDs(seqTo);
		if ( idsTo != null && !idsTo.isEmpty() && !idsTo.contains(id) ) {
			logger.info("The converted {} is already registered in {} (normalized)", formatTo, idsTo);
			repo.setIDsForToSequence(idsTo, true);
		}

		return repo;
	}

	protected abstract List<String> getFromSequences(String id) throws IOException;
	protected abstract List<String> getToSequences(String id) throws IOException;
	protected abstract List<String> getIDsForToSequence(String toSeq) throws IOException;
	protected abstract String convert(String gct) throws Exception;
	protected abstract String getValidationMessages();

}
