package org.glycoinfo.SequenceRegistrationInspection.reporter.om;

import java.util.List;

public class ConversionReport implements IReport {

	private String id;
	private String seqFrom;
	private String seqTo;
	private String seqToOrig;
	private List<String> idsForSeqTo;
	private List<String> idsForSeqToNorm;

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}

	public String getFromSequence() {
		return seqFrom;
	}

	public void setFromSequence(String seqFrom) {
		this.seqFrom = seqFrom;
	}

	public String getToSequence() {
		return seqTo;
	}

	public void setToSequence(String seqTo) {
		this.seqTo = seqTo;
	}

	public String getToOriginalSequence() {
		return seqToOrig;
	}

	public void setToOriginalSequence(String seqToOrig) {
		this.seqToOrig = seqToOrig;
	}

	public List<String> getIDsForToSequence(boolean isNormalized) {
		return (isNormalized)? idsForSeqToNorm : idsForSeqTo;
	}

	public void setIDsForToSequence(List<String> idsForSeqTo, boolean isNormalized) {
		if ( isNormalized )
			this.idsForSeqToNorm = idsForSeqTo;
		else
			this.idsForSeqTo = idsForSeqTo;
	}

	@Override
	public String[] toStringElements() {
		String[] elems = new String[6];
		elems[0] = this.id;
		elems[1] = this.seqFrom;
		elems[2] = this.seqTo;
		elems[3] = this.seqToOrig;
		elems[4] = (this.idsForSeqTo != null)? String.join(",", this.idsForSeqTo) : "";
		elems[5] = (this.idsForSeqToNorm != null)? String.join(",", this.idsForSeqToNorm) : "";
		return elems;
	}
}
