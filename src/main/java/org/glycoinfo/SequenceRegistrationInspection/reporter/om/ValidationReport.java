package org.glycoinfo.SequenceRegistrationInspection.reporter.om;

import java.util.List;

public class ValidationReport implements IReport {

	private String seq;
	private List<String> ids;
	private Exception exception;
	private String messages;

	public String getSequence() {
		return seq;
	}

	public void setSequence(String seq) {
		this.seq = seq;
	}

	public List<String> getIDs() {
		return ids;
	}

	public void setIDs(List<String> ids) {
		this.ids = ids;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public String getMessages() {
		return messages;
	}

	public void setMessages(String messages) {
		this.messages = messages;
	}

	@Override
	public String[] toStringElements() {
		String[] elems = new String[4];
		elems[0] = this.seq;
		elems[1] = (this.ids != null)? String.join(",", this.ids) : "";
		elems[2] = (this.exception != null)? this.exception.getMessage() : "";
		elems[3] = this.messages;
		return elems;
	}
}
