package org.glycoinfo.SequenceRegistrationInspection.reporter.om;

public interface IReport {

	public String[] toStringElements();
}
