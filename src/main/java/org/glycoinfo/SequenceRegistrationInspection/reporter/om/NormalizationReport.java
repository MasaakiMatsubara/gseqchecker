package org.glycoinfo.SequenceRegistrationInspection.reporter.om;

import java.util.List;

public class NormalizationReport implements IReport {

	private String seqOrig;
	private String seqNorm;
	private List<String> lOrigIDs;
	private List<String> lNormIDs;
	private String messages;

	public String getOriginalSequence() {
		return seqOrig;
	}

	public void setOriginalSequence(String seqOrig) {
		this.seqOrig = seqOrig;
	}

	public String getNormalizedSequence() {
		return seqNorm;
	}

	public void setNormalizedSequence(String seqNorm) {
		this.seqNorm = seqNorm;
	}

	public void setIDsForOriginal(List<String> lIDs) {
		this.lOrigIDs = lIDs;
	}

	public List<String> getIDsForOriginal() {
		return this.lOrigIDs;
	}

	public void setIDsForNormalized(List<String> lIDs) {
		this.lNormIDs = lIDs;
	}

	public List<String> getIDsForNormalized() {
		return this.lNormIDs;
	}

	public String getMessages() {
		return messages;
	}

	public void setMessages(String messages) {
		this.messages = messages;
	}

	@Override
	public String[] toStringElements() {
		String[] elems  = new String[5];
		elems[0] = this.seqOrig;
		elems[1] = this.seqNorm;
		elems[2] = (this.lOrigIDs != null)? String.join(",", this.lOrigIDs) : "";
		elems[3] = (this.lNormIDs != null)? String.join(",", this.lNormIDs) : "";
		elems[4] = this.messages;
		return elems;
	}
}
