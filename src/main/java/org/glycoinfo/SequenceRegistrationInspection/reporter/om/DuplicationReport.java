package org.glycoinfo.SequenceRegistrationInspection.reporter.om;

import java.util.List;

public class DuplicationReport implements IReport {

	private String seq;
	private List<String> lDupIDs;

	public String getSequence() {
		return seq;
	}

	public void setSequence(String seq) {
		this.seq = seq;
	}

	public List<String> getDuplicatedIDs() {
		return lDupIDs;
	}

	public void setDuplicatedIDs(List<String> lDupIDs) {
		this.lDupIDs = lDupIDs;
	}

	@Override
	public String[] toStringElements() {
		String[] elems = new String[2];
		elems[0] = this.seq;
		elems[1] = (this.lDupIDs != null)? String.join(",", this.lDupIDs) : "";
		return elems;
	}
}
