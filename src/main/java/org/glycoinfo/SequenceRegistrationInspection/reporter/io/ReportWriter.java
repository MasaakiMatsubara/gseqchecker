package org.glycoinfo.SequenceRegistrationInspection.reporter.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.SequenceRegistrationInspection.reporter.om.IReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportWriter {
	private static final Logger logger = LoggerFactory.getLogger(ReportWriter.class);

	private List<? extends IReport> reports;

	public ReportWriter() {
		clearReport();
	}

	public void clearReport() {
		this.reports = new ArrayList<>();
	}

	public void setReports(List<? extends IReport> repos) {
		this.reports = repos;
	}

	public void writeReports(String filePath) {
		try (BufferedOutputStream bufferedOutputStream =
				new BufferedOutputStream(new FileOutputStream(new File(filePath)))) {

			for ( IReport repo : this.reports ) {
				String[] line = repo.toStringElements();
				// Double quotes elements
				for ( int i=0; i<line.length; i++ )
					line[i] = "\""+line[i]+"\"";
				bufferedOutputStream.write(String.join("\t", line).getBytes());
				bufferedOutputStream.write(System.getProperty("line.separator").getBytes());
			}
		} catch (IOException e) {
			logger.error("An error in writing report to "+filePath, e);
		}
	}
}
