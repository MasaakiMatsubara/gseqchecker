package org.glycoinfo.GSeqConsistencyChecker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SequenceLoader {
	private static final Logger logger = LoggerFactory.getLogger(SequenceLoader.class);

	private static class Sequence {
		private String id;
		private List<String> lWURCSs;
		private List<String> lSeqs;

		public Sequence(String id, String wurcs, String seq) {
			this.id = id;
			this.lWURCSs = new ArrayList<>();
			if ( wurcs != null )
				this.lWURCSs.add(wurcs);
			this.lSeqs = new ArrayList<>();
			if ( seq != null )
				this.lSeqs.add(seq);
		}

		public String getID() {
			return id;
		}

		public void addWURCS(String wurcs) {
			if ( wurcs != null && !this.lWURCSs.contains(wurcs) )
				this.lWURCSs.add(wurcs);
		}

		public List<String> getWURCSs() {
			return lWURCSs;
		}

		public void addSequence(String seq) {
			if ( seq != null && !this.lSeqs.contains(seq) )
				this.lSeqs.add(seq);
		}

		public List<String> getSequences() {
			return lSeqs;
		}
	}

	private static List<String> lIDs;
	private static List<String> lWURCSs;
	private static List<String> lSeqs;
	private static Map<String, Sequence> mapIDToSeq;
	private static Map<String, List<String>> mapWURCSToIDs;
	private static Map<String, List<String>> mapSeqToIDs;

	public static List<String> getIDs() {
		return lIDs;
	}

	public static List<String> getWURCSs() {
		return lWURCSs;
	}

	public static List<String> getSeqs() {
		return lSeqs;
	}

	public static List<String> getWURCSs(String id) throws IOException {
		if ( getSequenceSet(id) == null || getSequenceSet(id).getWURCSs().isEmpty() ) {
			logger.warn("No WURCS for {}", id);
			return null;
		}
		return getSequenceSet(id).getWURCSs();
	}

	public static List<String> getSequences(String id) throws IOException {
		if ( getSequenceSet(id) == null || getSequenceSet(id).getSequences().isEmpty() ) {
			logger.warn("No Sequence for {}", id);
			return null;
		}
		return getSequenceSet(id).getSequences();
	}

	private static Sequence getSequenceSet(String id) throws IOException {
		if (id == null || id.isEmpty()) {
			logger.warn("No GlyTouCan ID");
			return null;
		}

		return lookupSequenceSet(id);
	}

	private static Sequence lookupSequenceSet(String id) {
		if (mapIDToSeq == null) {
			throw new RuntimeException("Mapping file is not set");
		}

		return mapIDToSeq.get(id);
	}

	public static List<String> getIDsForWURCS(String wurcs) throws IOException {
		if (wurcs == null || wurcs.isEmpty()) {
			logger.warn("No WURCS");
			return null;
		}

		return lookupIDsForWURCS(wurcs);
	}

	public static List<String> getIDsForSequence(String seq) throws IOException {
		if (seq == null || seq.isEmpty()) {
			logger.warn("No Seq");
			return null;
		}

		return lookupIDsForSequence(seq);
	}

	private static List<String> lookupIDsForWURCS(String wurcs) {
		if (mapWURCSToIDs == null) {
			throw new RuntimeException("Mapping file is not set");
		}

		return mapWURCSToIDs.get(wurcs);
	}

	private static List<String> lookupIDsForSequence(String seq) {
		if (mapSeqToIDs == null) {
			throw new RuntimeException("Mapping file is not set");
		}

		return mapSeqToIDs.get(seq);
	}

	public static void load(Path file) throws IOException {
		List<String[]> trios = new ArrayList<>();
		try (BufferedReader reader = newBufferedReader(file.toFile())) {
			final StringBuffer line = new StringBuffer();
			reader.lines().forEach(x -> {
				line.append(x);
				if (!x.endsWith("\"")) {
					line.append("\n");
					return;
				}

				String[] trio = line.toString().split("\t");
				if ( trio.length != 3 )
					throw new RuntimeException("The number of items per row must be three.");
				for ( int i=0; i<trio.length; i++ ) {
					trio[i] = trio[i].replaceAll("^\"", "").replaceAll("\"$", "");
					if ( trio[i].isEmpty() || trio[i].equals("-") )
						trio[i] = null;
				}
				trios.add(trio);
				line.delete(0, line.length());
			});
		}

		boolean bDup = false;

		Map<String, Sequence> mapping = new HashMap<>();
		List<String> list = new ArrayList<>();
		for ( String[] trio : trios ) {
			String id = trio[0];
			String wurcs = trio[1];
			String seq = trio[2];

			if ( id == null ) {
				logger.error("No ID entry is found");
				continue;
			}

			if ( wurcs == null )
				logger.warn("No WURCS in {}", id);
			if ( seq == null )
				logger.warn("No sequence in {}", id);

			Sequence oSeq = null;
			if ( !mapping.containsKey(id) ) {
				list.add(id);
				oSeq = new Sequence(id, wurcs, seq);
				mapping.put(id, oSeq);
				continue;
			}
			oSeq = mapping.get(id);
			oSeq.addWURCS(wurcs);
			oSeq.addSequence(seq);
			if ( oSeq.getWURCSs().size() == 2 ){
				logger.warn("Duplicated WURCS is found in {}", id);
				bDup = true;
			}
			if ( oSeq.getSequences().size() == 2 ){
				logger.warn("Duplicated sequence is found in {}", id);
				bDup = true;
			}
		}
		mapIDToSeq = mapping;
		lIDs = list;

		List<String> list1 = new ArrayList<>();
		List<String> list2 = new ArrayList<>();
		Map<String, List<String>> mapping1 = new HashMap<>();
		Map<String, List<String>> mapping2 = new HashMap<>();
		for ( String id : list ) {
			Sequence oSeq = mapping.get(id);
			for ( String wurcs : oSeq.getWURCSs() ) {
				if ( !list1.contains(wurcs) )
					list1.add(wurcs);
				if ( !mapping1.containsKey(wurcs) )
					mapping1.put(wurcs, new ArrayList<>());
				mapping1.get(wurcs).add(id);
			}
			for ( String seq : oSeq.getSequences() ) {
				if ( !list2.contains(seq) )
					list2.add(seq);
				if ( !mapping2.containsKey(seq) )
					mapping2.put(seq, new ArrayList<>());
				mapping2.get(seq).add(id);
			}
		}
		lWURCSs = list1;
		lSeqs = list2;
		mapWURCSToIDs = mapping1;
		mapSeqToIDs = mapping2;

		logger.info("Successfully loaded {}", file.toAbsolutePath().toString());
		logger.info("Total # of lines: {}", trios.size());
		logger.info("Total # of entries: {}", lIDs.size());
		logger.info("Total # of WURCSs: {}", lWURCSs.size());
		logger.info("Total # of sequences: {}", lSeqs.size());

		if ( !bDup )
			return;

		logger.warn("Duplication check:");
		for ( String id : list ) {
			Sequence oSeq = mapIDToSeq.get(id);
			if ( oSeq.getWURCSs().size() > 1 )
				logger.warn("Duplicated WURCSs in {}: {}", id, oSeq.getWURCSs());
			if ( oSeq.getSequences().size() > 1 )
				logger.warn("Duplicated Sequences in {}: {}", id, oSeq.getSequences());
		}
	}

	public static BufferedReader newBufferedReader(File file) throws IOException {
		return newBufferedReader(file, StandardCharsets.UTF_8);
	}

	public static BufferedReader newBufferedReader(File file, Charset cs) throws IOException {
		return new BufferedReader(new InputStreamReader(new FileInputStream(file), cs));
	}

}
